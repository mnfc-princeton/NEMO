#!/bin/bash

# Run migrations to create or update the database
django-admin makemigrations NEMO
django-admin migrate

# Collect static files
django-admin collectstatic --no-input --clear
# Run NEMO
if (-not $env:WAITRESS_THREADS) {
    $env:WAITRESS_THREADS = "8"
}
waitress-serve --threads=$env:WAITRESS_THREADS --port=8000 NEMO.wsgi:application
