from django.core.management import BaseCommand

from NEMO.views.timed_services import send_email_daily_passdown


class Command(BaseCommand):
    help = "Run every day at 8am to send daily passdown"

    def handle(self, *args, **options):
        send_email_daily_passdown()
