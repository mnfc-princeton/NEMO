from django.core.management import BaseCommand

from NEMO.views.timed_services import send_email_user_chem_reminders


class Command(BaseCommand):
    help = "Run every day at 8:15am to send daily expired chemical reminder"

    def handle(self, *args, **options):
        send_email_user_chem_reminders()
