from django.core.management import BaseCommand

from NEMO.views.intercom import buddy_alert_announcement


class Command(BaseCommand):
    help = "Run every 1/2 hour to trigger audio buddy announcement when there are less than 2 users in a buddy-required area. "

    def add_arguments(self, parser):
        parser.add_argument("area_id", type=int, help="id of the area")

    def handle(self, *args, **options):
        buddy_alert_announcement(options.get("area_id"))
