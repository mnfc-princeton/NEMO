from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("NEMO", "0045_version_4_5_0"),
    ]

    def set_area_customization(apps, schema_editor):
        Customization = apps.get_model("NEMO", "Customization")
        Customization.objects.create(name="remote_work_start_area_access_automatically", value="off")

    operations = [
        migrations.RunPython(set_area_customization),
    ]
