from logging import getLogger
from pathlib import Path

from django.contrib.auth.decorators import login_required
from django.core.files.storage import get_storage_class
from django.shortcuts import render
from django.views.decorators.http import require_http_methods

from NEMO.models import User
from NEMO.utilities import parse_parameter_string, render_email_template, send_mail
from NEMO.views.constants import FEEDBACK_MAXIMUM_LENGTH
from NEMO.views.customization import EmailsCustomization, get_media_file_contents

consultation_logger = getLogger(__name__)


@login_required
@require_http_methods(["GET", "POST"])
def consultation(request):
    user: User = request.user
    recipient = EmailsCustomization.get("feedback_email_address")
    email_contents = get_media_file_contents("consultation_email.html")
    email_response_contents = get_media_file_contents("consultation_email_response.html")
    if not recipient or not email_contents or not email_response_contents:
        return render(request, "feedback.html", {"customization_required": True})

    if request.method == "GET":
        return render(request, "consultation.html")
    contents = parse_parameter_string(request.POST, "consultation", FEEDBACK_MAXIMUM_LENGTH)
    if contents == "":
        return render(request, "consultation.html")
    dictionary = {
        "contents": contents,
        "user": user,
    }

    email = render_email_template(email_contents, dictionary, request)
    send_mail(
        subject="Consultation Request from " + str(user), content=email, from_email=request.user.email, to=[recipient]
    )

    email_response = render_email_template(email_response_contents, dictionary, request)
    storage = get_storage_class()()

    attachments = []
    try:
        path = Path(storage.path("design_consultation_template.pptx"))
        with path.open("rb") as file:
            content = file.read()
            attachments.append([path.name, content])
    except Exception as e:
        consultation_logger.exception(e)
    user.email_user(
        "Design Consultation Request Follow Up", message=email_response, from_email=recipient, attachments=attachments
    )

    dictionary = {
        "title": "Design Consultation Request",
        "heading": "Request Sent!",
        "content": "Your design consultation request has been sent to the staff. We will follow up with you as soon as we can.",
    }
    return render(request, "acknowledgement.html", dictionary)
