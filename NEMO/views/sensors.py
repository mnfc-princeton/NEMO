from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render
from pymodbus.client import ModbusTcpClient

from NEMO.decorators import disable_session_expiry_refresh
from NEMO.models import Sensor
from NEMO.utilities import send_mail
from NEMO.views.customization import EmailsCustomization


@staff_member_required
@disable_session_expiry_refresh
def read_sensor_data(request):
    sensors = Sensor.objects.all()
    data = []
    dictionary = {}
    for sensor in sensors:
        client = ModbusTcpClient(sensor.address.server, port=502, timeout=1)
        try:
            client.connect()
            if sensor.sensor_type == Sensor.SensorType.DIGITAL:
                reply = client.read_discrete_inputs(sensor.channel, 1, slave=1)
                sensor_reading = reply.bits[0]
                sensor_response = {"sensor_name": sensor.name, "sensor_reading": sensor_reading}
                if sensor.digital_sensor_alert and sensor.email and str(sensor_reading) != sensor.last_value:
                    if sensor.digital_alert_value and sensor_reading:
                        send_sensor_alert_email(sensor, str(sensor_reading))
                    elif not sensor.digital_alert_value and not sensor_reading:
                        send_sensor_alert_email(sensor, str(sensor_reading))
            else:
                reply = client.read_input_registers(sensor.channel, 1, slave=1)
                sensor_reading = round(reply.registers[0] * sensor.conversion_factor, 2)
                if sensor.high_alert_value and sensor.email:
                    if sensor_reading > sensor.high_alert_value >= float(sensor.last_value):
                        send_sensor_alert_email(sensor, str(sensor_reading))
                if sensor.low_alert_value and sensor.email:
                    if sensor_reading < sensor.low_alert_value <= float(sensor.last_value):
                        send_sensor_alert_email(sensor, str(sensor_reading))
            sensor.last_value = str(sensor_reading)
            sensor.save()
            client.close()
        except:
            sensor_reading = "Could Not Connect"
        sensor_response = {"sensor_name": sensor.name, "sensor_reading": sensor_reading}
        data.append(sensor_response)
        client.close()
    dictionary["sensor_data"] = data
    return render(request, "sensor_data/sensor_data.html", dictionary)


@staff_member_required
def sensor_data(request):
    return render(request, "sensor_data/sensors.html")


def send_sensor_alert_email(sensor, value):
    try:
        user_office_email = EmailsCustomization.get("user_office_email_address")
        subject = f"Sensor Alert for {sensor.name}"
        message = (
            f"The following sensor has recorded a value past its alert threshold: {sensor.name} with value {value}"
        )
        send_mail(subject=subject, content=message, from_email=user_office_email, to=[sensor.email])
    except:
        pass


#
# client = ModbusTcpClient(#server#)
# client.connect()
# client.write_coil(#channel, command, unit=1)
# client.read_coils(#channel, number of coils to read, unit=1)
# client.read_input_registers()
#
# response = client.read_holding_registers(
#     address=register,  # 40161
#     count=count,       # 2
#     unit=device)       # 4
#
# decoder = BinaryPayloadDecoder.fromRegisters(
#     registers=response.registers,
#     byteorder=Endian.Big,
#     wordorder=Endian.Big)
#
# value = decoder.decode_32bit_float()
#
# client.close()
