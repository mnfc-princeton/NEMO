from collections import defaultdict
from logging import getLogger
from typing import Callable, List, Set, Union

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.views.decorators.http import require_GET
from requests import get
from xlsxwriter.workbook import Workbook

from NEMO.decorators import accounting_or_user_office_or_manager_required
from NEMO.models import (
    Account,
    AdjustmentRequest,
    AreaAccessRecord,
    ConsumableWithdraw,
    Project,
    Reservation,
    StaffCharge,
    StockroomWithdraw,
    Tool,
    TrainingSession,
    UsageEvent,
    User,
)
from NEMO.utilities import (
    BasicDisplayTable,
    export_format_datetime,
    extract_optional_beginning_and_end_dates,
    get_day_timeframe,
    get_month_timeframe,
    month_list,
)
from NEMO.views.api_billing import (
    BillableItem,
    billable_items_area_access_records,
    billable_items_consumable_withdrawals,
    billable_items_missed_reservations,
    billable_items_staff_charges,
    billable_items_training_sessions,
    billable_items_usage_events,
)
from NEMO.views.customization import ProjectsAccountsCustomization, UserRequestsCustomization

logger = getLogger(__name__)


# Class for Applications that can be used for autocomplete
class Application(object):
    def __init__(self, name):
        self.name = name
        self.id = name

    def __str__(self):
        return self.name


# Class for Categories that can be used for autocomplete
class Category(Application):
    pass


# We want to keep all the parameters of the request when switching tabs, so we are just replacing usage <-> billing urls
def get_url_for_other_tab(request):
    full_path_request = request.get_full_path()
    usage_url = reverse("usage")
    billing_url = reverse("usage_billing")
    project_usage_url = reverse("project_usage")
    project_billing_url = reverse("project_billing")
    if project_usage_url in full_path_request:
        full_path_request = full_path_request.replace(project_usage_url, project_billing_url)
    elif project_billing_url in full_path_request:
        full_path_request = full_path_request.replace(project_billing_url, project_usage_url)
    elif usage_url in full_path_request:
        full_path_request = full_path_request.replace(usage_url, billing_url)
    elif billing_url in full_path_request:
        full_path_request = full_path_request.replace(billing_url, usage_url)
    return full_path_request


def get_project_applications():
    applications = []
    projects = Project.objects.filter(
        id__in=Project.objects.values("application_identifier").distinct().values_list("id", flat=True)
    )
    for project in projects:
        if not any(list(filter(lambda app: app.name == project.application_identifier, applications))):
            applications.append(Application(project.application_identifier))
    return applications


def get_tool_categories():
    categories = []
    for tool in Tool.objects.filter(visible=True).order_by("_category").values_list("_category").distinct():
        categories.append(Category(tool[0]))
    return categories


def date_parameters_dictionary(request, default_function: Callable = get_month_timeframe):
    if request.GET.get("start") and request.GET.get("end"):
        start_date, end_date = extract_optional_beginning_and_end_dates(request.GET, date_only=True)
    else:
        start_date, end_date = default_function()
    kind = request.GET.get("type")
    identifier = request.GET.get("id")
    customer = request.GET.get("customer")
    item_type = request.GET.get("item_type")
    item_id = request.GET.get("item_id")
    existing_adjustments = defaultdict(list)
    for values in (
        AdjustmentRequest.objects.filter(deleted=False, creator=request.user).values("item_type", "item_id").distinct()
    ):
        existing_adjustments[values["item_type"]].append(values["item_id"])
    dictionary = {
        "month_list": month_list(),
        "start_date": start_date,
        "end_date": end_date,
        "kind": kind,
        "identifier": identifier,
        "tab_url": get_url_for_other_tab(request) if get_billing_service().get("available", False) else "",
        "customer": None,
        "item": None,
        "billing_service": get_billing_service().get("available", False),
        "adjustment_time_limit": UserRequestsCustomization.get_date_limit(),
        "existing_adjustments": existing_adjustments,
    }
    user: User = request.user
    if user.is_any_part_of_staff:
        dictionary["users"] = User.objects.all()
        dictionary["tools"] = list(Tool.objects.all())
        dictionary["categories"] = get_tool_categories()
    try:
        if customer:
            dictionary["customer"] = User.objects.get(id=customer)
        if item_type and item_id:
            if item_type == "tool":
                dictionary["item"] = Tool.objects.get(id=item_id)
            elif item_type == "category":
                dictionary["item"] = Category(item_id)
    except:
        pass
    return dictionary, start_date, end_date, kind, identifier


# Unused but kept so merging is easier
@login_required
@require_GET
def nist_usage(request):
    user: User = request.user
    user_managed_projects = get_managed_projects(user)
    base_dictionary, start_date, end_date, kind, identifier = date_parameters_dictionary(request, get_month_timeframe)
    customer_filter = Q(customer=user) | Q(project__in=user_managed_projects)
    user_filter = Q(user=user) | Q(project__in=user_managed_projects)
    trainee_filter = Q(trainee=user) | Q(project__in=user_managed_projects)
    project_id = request.GET.get("project") or request.GET.get("pi_project")
    csv_export = bool(request.GET.get("csv", False))
    show_only_my_usage = user_managed_projects and request.GET.get("show_only_my_usage", "enabled") == "enabled"
    if show_only_my_usage:
        # Forcing to be user only
        customer_filter &= Q(customer=user)
        user_filter &= Q(user=user)
        trainee_filter &= Q(trainee=user)
    if user_managed_projects:
        base_dictionary["selected_project"] = "all"
    if project_id:
        project = get_object_or_404(Project, id=project_id)
        if request.GET.get("project"):
            base_dictionary["selected_user_project"] = project
        else:
            base_dictionary["selected_project"] = project
        customer_filter = customer_filter & Q(project=project)
        user_filter = user_filter & Q(project=project)
        trainee_filter = trainee_filter & Q(project=project)
    area_access = (
        AreaAccessRecord.objects.filter(customer_filter)
        .filter(end__gt=start_date, end__lte=end_date)
        .order_by("-start")
    )
    consumables = ConsumableWithdraw.objects.filter(customer_filter).filter(date__gt=start_date, date__lte=end_date)
    missed_reservations = Reservation.objects.filter(user_filter).filter(
        missed=True, end__gt=start_date, end__lte=end_date
    )
    staff_charges = StaffCharge.objects.filter(customer_filter).filter(end__gt=start_date, end__lte=end_date)
    training_sessions = TrainingSession.objects.filter(trainee_filter).filter(date__gt=start_date, date__lte=end_date)
    usage_events = UsageEvent.objects.filter(user_filter).filter(end__gt=start_date, end__lte=end_date)
    if csv_export:
        return csv_export_response(
            user, usage_events, area_access, training_sessions, staff_charges, consumables, missed_reservations
        )
    else:
        dictionary = {
            "area_access": area_access,
            "consumables": consumables,
            "missed_reservations": missed_reservations,
            "staff_charges": staff_charges,
            "training_sessions": training_sessions,
            "usage_events": usage_events,
        }
        if user_managed_projects:
            dictionary["pi_projects"] = user_managed_projects
            dictionary["show_only_my_usage"] = show_only_my_usage
        dictionary["no_charges"] = not (
            dictionary["area_access"]
            or dictionary["consumables"]
            or dictionary["missed_reservations"]
            or dictionary["staff_charges"]
            or dictionary["training_sessions"]
            or dictionary["usage_events"]
        )
        return render(request, "usage/usage.html", {**base_dictionary, **dictionary})


@login_required
@require_GET
def usage(request):
    user: User = request.user
    user_managed_projects = get_managed_projects(user)
    base_dictionary, start_date, end_date, kind, identifier = date_parameters_dictionary(request)
    dictionary = get_usage(start_date, end_date, user=user)
    dictionary["no_charges"] = not (
        dictionary["area_access"]
        or dictionary["stockroom_purchases"]
        or dictionary["missed_reservations"]
        or dictionary["staff_charges"]
        or dictionary["training_sessions"]
        or dictionary["usage_events"]
    )
    return render(request, "usage/usage.html", {**base_dictionary, **dictionary})


@login_required
@require_GET
def usagexlsx(request):
    base_dictionary, start_date, end_date, kind, identifier = date_parameters_dictionary(request)
    if not request.user.is_staff:
        user = request.user
    elif base_dictionary["customer"] and request.user.is_staff:
        user = base_dictionary["customer"]
    else:
        user = None
    item = None
    projects = []
    if request.user.is_staff:
        selection = ""
        try:
            if kind == "application":
                projects = Project.objects.filter(application_identifier=identifier)
                selection = identifier
            elif kind == "project":
                projects = [Project.objects.get(id=identifier)]
                selection = projects[0].name
            elif kind == "account":
                account = Account.objects.get(id=identifier)
                projects = Project.objects.filter(account=account)
                selection = account.name
        except:
            pass
        item = base_dictionary["item"]

    dictionary = get_usage(start_date, end_date, user, projects, item)
    area_access = (
        dictionary["area_access"].values(
            "customer__username",
            "customer__first_name",
            "customer__last_name",
            "area__name",
            "start",
            "end",
            "project__name",
            "project__account__name",
        )
        if dictionary["area_access"]
        else None
    )
    usage_record = (
        dictionary["usage_events"].values(
            "user__username",
            "user__first_name",
            "user__last_name",
            "operator__username",
            "tool__name",
            "tool___category",
            "start",
            "end",
            "project__name",
            "project__account__name",
            "validated",
        )
        if dictionary["usage_events"]
        else None
    )
    stockroom_purchases = (
        dictionary["stockroom_purchases"].values(
            "customer__username",
            "customer__first_name",
            "customer__last_name",
            "stock__name",
            "stock__cost",
            "quantity",
            "date",
            "project__name",
            "project__account__name",
        )
        if dictionary["stockroom_purchases"]
        else None
    )
    staff_charges = (
        dictionary["staff_charges"].values(
            "staff_member__username",
            "customer__username",
            "customer__first_name",
            "customer__last_name",
            "start",
            "end",
            "validated",
            "project__name",
            "project__account__name",
        )
        if dictionary["staff_charges"]
        else None
    )
    date_format_str = "yyyy/mm/dd hh:mm"
    fn = "usage_report" + "_" + start_date.strftime("%Y%m%d") + "_" + end_date.strftime("%Y%m%d") + ".xlsx"
    response = HttpResponse(content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response["Content-Disposition"] = 'attachment; filename = "%s"' % fn
    book = Workbook(response, {"in_memory": True, "remove_timezone": True})
    date_format = book.add_format({"num_format": date_format_str, "align": "left"})
    bold = book.add_format({"bold": True})
    if area_access:
        area_sheet = book.add_worksheet("Area Access")
        area_sheet.set_column(0, 5, 18)
        area_sheet.set_column(6, 6, 40)
        area_sheet.set_column(7, 7, 20)
        headings = ["Name", "Username", "Area", "Start", "End", "Duration (Hours)", "Project", "Account"]
        area_sheet.write_row(0, 0, headings, bold)
        for row_num, row_data in enumerate(area_access):
            duration = (row_data["end"] - row_data["start"]).total_seconds() / 60 / 60
            full_name = row_data["customer__last_name"] + ", " + row_data["customer__first_name"]
            area_sheet.write(row_num + 1, 0, full_name)
            area_sheet.write(row_num + 1, 1, row_data["customer__username"])
            area_sheet.write(row_num + 1, 2, row_data["area__name"])
            area_sheet.write_datetime(row_num + 1, 3, (row_data["start"]).astimezone(tz=None), date_format)
            area_sheet.write_datetime(row_num + 1, 4, (row_data["end"]).astimezone(tz=None), date_format)
            area_sheet.write(row_num + 1, 5, duration)
            area_sheet.write(row_num + 1, 6, row_data["project__name"])
            area_sheet.write(row_num + 1, 7, row_data["project__account__name"])

    if usage_record:
        usage_sheet = book.add_worksheet("Tool Usage")
        usage_sheet.set_column(0, 2, 20)
        usage_sheet.set_column(3, 3, 30)
        usage_sheet.set_column(4, 7, 18)
        usage_sheet.set_column(8, 8, 30)
        usage_sheet.set_column(9, 9, 18)
        headings = [
            "Name",
            "Username",
            "Operator",
            "Tool",
            "Category",
            "Start",
            "End",
            "Duration (Hours)",
            "Validated Assisted Use",
            "Project",
            "Account",
        ]
        usage_sheet.write_row(0, 0, headings, bold)
        for row_num, row_data in enumerate(usage_record):
            duration = (row_data["end"] - row_data["start"]).total_seconds() / 60 / 60
            full_name = row_data["user__last_name"] + ", " + row_data["user__first_name"]
            usage_sheet.write(row_num + 1, 0, full_name)
            usage_sheet.write(row_num + 1, 1, row_data["user__username"])
            usage_sheet.write(row_num + 1, 2, row_data["operator__username"])
            usage_sheet.write(row_num + 1, 3, row_data["tool__name"])
            usage_sheet.write(row_num + 1, 4, row_data["tool___category"])
            usage_sheet.write_datetime(row_num + 1, 5, (row_data["start"]).astimezone(tz=None), date_format)
            usage_sheet.write_datetime(row_num + 1, 6, (row_data["end"]).astimezone(tz=None), date_format)
            usage_sheet.write(row_num + 1, 7, duration)
            usage_sheet.write(
                row_num + 1,
                8,
                row_data["validated"] if row_data["user__username"] != row_data["operator__username"] else "",
            )
            usage_sheet.write(row_num + 1, 9, row_data["project__name"])
            usage_sheet.write(row_num + 1, 10, row_data["project__account__name"])

    if stockroom_purchases:
        stockroom_sheet = book.add_worksheet("Stockroom Purchases")
        stockroom_sheet.set_column(0, 1, 16)
        stockroom_sheet.set_column(2, 2, 30)
        stockroom_sheet.set_column(3, 5, 18)
        stockroom_sheet.set_column(6, 7, 40)
        headings = ["Name", "Username", "Item", "Quantity", "Unit Price", "Date", "Project", "Account"]
        stockroom_sheet.write_row(0, 0, headings, bold)
        for row_num, row_data in enumerate(stockroom_purchases):
            full_name = row_data["customer__last_name"] + ", " + row_data["customer__first_name"]
            stockroom_sheet.write(row_num + 1, 0, full_name)
            stockroom_sheet.write(row_num + 1, 1, row_data["customer__username"])
            stockroom_sheet.write(row_num + 1, 2, row_data["stock__name"])
            stockroom_sheet.write(row_num + 1, 3, row_data["quantity"])
            stockroom_sheet.write(row_num + 1, 4, row_data["stock__cost"])
            stockroom_sheet.write_datetime(row_num + 1, 5, (row_data["date"]).astimezone(tz=None), date_format)
            stockroom_sheet.write(row_num + 1, 6, row_data["project__name"])
            stockroom_sheet.write(row_num + 1, 7, row_data["project__account__name"])

    if staff_charges:
        staffcharge_sheet = book.add_worksheet("Staff Charges")
        staffcharge_sheet.set_column(0, 6, 18)
        staffcharge_sheet.set_column(7, 7, 40)
        staffcharge_sheet.set_column(8, 8, 20)
        headings = [
            "Name",
            "Username",
            "Staff Member",
            "Start",
            "End",
            "Duration (Hours)",
            "Validated",
            "Project",
            "Account",
        ]
        staffcharge_sheet.write_row(0, 0, headings, bold)
        for row_num, row_data in enumerate(staff_charges):
            full_name = row_data["customer__last_name"] + ", " + row_data["customer__first_name"]
            duration = (row_data["end"] - row_data["start"]).total_seconds() / 60 / 60
            staffcharge_sheet.write(row_num + 1, 0, full_name)
            staffcharge_sheet.write(row_num + 1, 1, row_data["customer__username"])
            staffcharge_sheet.write(row_num + 1, 2, row_data["staff_member__username"])
            staffcharge_sheet.write_datetime(row_num + 1, 3, (row_data["start"]).astimezone(tz=None), date_format)
            staffcharge_sheet.write_datetime(row_num + 1, 4, (row_data["end"]).astimezone(tz=None), date_format)
            staffcharge_sheet.write(row_num + 1, 5, duration)
            staffcharge_sheet.write(row_num + 1, 6, row_data["validated"])
            staffcharge_sheet.write(row_num + 1, 7, row_data["project__name"])
            staffcharge_sheet.write(row_num + 1, 8, row_data["project__account__name"])

    if request.user.is_staff:
        filter_parameters = book.add_worksheet("Filter Parameters")
        filter_parameters.write(0, 0, "Filter Parameters", bold)
        filter_parameters.write_row(
            1, 0, ["Start", "End", "User", "Category" if isinstance(item, Category) else "Tool", "Projects"], bold
        )
        filter_parameters.write(2, 0, start_date, date_format)
        filter_parameters.write(2, 1, end_date, date_format)
        if user:
            filter_parameters.write(2, 2, user.username)
        if item:
            filter_parameters.write(2, 3, item.name)
        if projects:
            for count, proj in enumerate(projects):
                filter_parameters.write(2 + count, 4, proj.name)
    book.close()

    return response


@login_required
@require_GET
def billing(request):
    user: User = request.user
    base_dictionary, start_date, end_date, kind, identifier = date_parameters_dictionary(request, get_month_timeframe)
    if not base_dictionary["billing_service"]:
        return redirect("usage")
    user_project_applications = list(user.active_projects().values_list("application_identifier", flat=True)) + list(
        user.managed_projects.values_list("application_identifier", flat=True)
    )
    formatted_applications = ",".join(map(str, set(user_project_applications)))
    try:
        billing_dictionary = billing_dict(start_date, end_date, user, formatted_applications)
        return render(request, "usage/billing.html", {**base_dictionary, **billing_dictionary})
    except Exception as e:
        logger.warning(str(e))
        return render(request, "usage/billing.html", base_dictionary)


def get_usage(start_date, end_date, user=None, projects=None, item: Union[Tool, Category] = None):

    area_access = None
    usage_events = None
    missed_reservations = None
    staff_charges = None
    stockroom_purchases = None
    training_sessions = None

    try:
        if item and isinstance(item, (Tool, Category)):
            usage_events = UsageEvent.objects.filter(end__gt=start_date, end__lte=end_date).order_by("end")
            missed_reservations = Reservation.objects.filter(
                missed=True, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            training_sessions = TrainingSession.objects.filter(date__gt=start_date, date__lte=end_date).order_by("date")
            if isinstance(item, Tool):
                usage_events = usage_events.filter(tool=item.id)
                missed_reservations = missed_reservations.filter(tool=item.id)
                training_sessions = training_sessions.filter(tool=item.id)
            elif isinstance(item, Category):
                usage_events = usage_events.filter(tool___category=item.id)
                missed_reservations = missed_reservations.filter(tool___category=item.id)
                training_sessions = training_sessions.filter(tool___category=item.id)
            if projects:
                usage_events = usage_events.filter(project__in=projects)
                missed_reservations = missed_reservations.filter(project__in=projects)
                training_sessions = training_sessions.filter(project__in=projects)
            if user:
                usage_events = usage_events.filter(user=user.id)
                missed_reservations = missed_reservations.filter(user=user.id)
                training_sessions = training_sessions.filter(trainee=user.id)
        elif projects:
            area_access = AreaAccessRecord.objects.filter(
                project__in=projects, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            usage_events = UsageEvent.objects.filter(
                project__in=projects, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            missed_reservations = Reservation.objects.filter(
                project__in=projects, missed=True, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            staff_charges = StaffCharge.objects.filter(
                project__in=projects, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            stockroom_purchases = StockroomWithdraw.objects.filter(
                project__in=projects, date__gt=start_date, date__lte=end_date
            ).order_by("date")
            training_sessions = TrainingSession.objects.filter(
                project__in=projects, date__gt=start_date, date__lte=end_date
            ).order_by("date")
            if user:
                area_access = area_access.filter(customer=user.id)
                usage_events = usage_events.filter(user=user.id)
                missed_reservations = missed_reservations.filter(user=user.id)
                staff_charges = staff_charges.filter(customer=user.id)
                stockroom_purchases = stockroom_purchases.filter(customer=user.id)
                training_sessions = training_sessions.filter(trainee=user.id)
        elif user:
            area_access = AreaAccessRecord.objects.filter(
                customer=user.id, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            usage_events = UsageEvent.objects.filter(user=user.id, end__gt=start_date, end__lte=end_date).order_by(
                "end"
            )
            missed_reservations = Reservation.objects.filter(
                user=user.id, missed=True, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            staff_charges = StaffCharge.objects.filter(
                customer=user.id, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            stockroom_purchases = StockroomWithdraw.objects.filter(
                customer=user.id, date__gt=start_date, date__lte=end_date
            ).order_by("date")
            training_sessions = TrainingSession.objects.filter(
                trainee=user.id, date__gt=start_date, date__lte=end_date
            ).order_by("date")
        else:
            # Get everything
            area_access = AreaAccessRecord.objects.filter(end__gt=start_date, end__lte=end_date).order_by("end")
            usage_events = UsageEvent.objects.filter(end__gt=start_date, end__lte=end_date).order_by("end")
            missed_reservations = Reservation.objects.filter(
                missed=True, end__gt=start_date, end__lte=end_date
            ).order_by("end")
            staff_charges = StaffCharge.objects.filter(end__gt=start_date, end__lte=end_date).order_by("end")
            stockroom_purchases = StockroomWithdraw.objects.filter(date__gt=start_date, date__lte=end_date).order_by(
                "date"
            )
            training_sessions = TrainingSession.objects.filter(date__gt=start_date, date__lte=end_date).order_by("date")
    except:
        pass

    dictionary = {
        "search_items": set(Account.objects.all())
        | set(Project.objects.all())
        | set(get_project_applications())
        | set(User.objects.all()),
        "area_access": area_access,
        "missed_reservations": missed_reservations,
        "staff_charges": staff_charges,
        "training_sessions": training_sessions,
        "usage_events": usage_events,
        "stockroom_purchases": stockroom_purchases,
    }

    return dictionary


@accounting_or_user_office_or_manager_required
@require_GET
def project_usage(request):
    base_dictionary, start_date, end_date, kind, identifier = date_parameters_dictionary(request, get_day_timeframe)

    # area_access, consumables, missed_reservations, staff_charges, training_sessions, usage_events = (
    #     None,
    #     None,
    #     None,
    #     None,
    #     None,
    #     None,
    # )

    projects = []
    user = None
    selection = ""
    try:
        if kind == "application":
            projects = Project.objects.filter(application_identifier=identifier)
            selection = identifier
        elif kind == "project":
            projects = [Project.objects.get(id=identifier)]
            selection = projects[0].name
        elif kind == "account":
            account = Account.objects.get(id=identifier)
            projects = Project.objects.filter(account=account)
            selection = account.name
        elif kind == "user":
            user = User.objects.get(id=identifier)
            projects = user.active_projects()
            selection = str(user)

        # Not used by Princeton, get_usage() is used instead
        # area_access = AreaAccessRecord.objects.filter(end__gt=start_date, end__lte=end_date).order_by("-start")
        # consumables = ConsumableWithdraw.objects.filter(date__gt=start_date, date__lte=end_date)
        # missed_reservations = Reservation.objects.filter(missed=True, end__gt=start_date, end__lte=end_date)
        # staff_charges = StaffCharge.objects.filter(end__gt=start_date, end__lte=end_date)
        # training_sessions = TrainingSession.objects.filter(date__gt=start_date, date__lte=end_date)
        # usage_events = UsageEvent.objects.filter(end__gt=start_date, end__lte=end_date)
        # if projects:
        #     area_access = area_access.filter(project__in=projects)
        #     consumables = consumables.filter(project__in=projects)
        #     missed_reservations = missed_reservations.filter(project__in=projects)
        #     staff_charges = staff_charges.filter(project__in=projects)
        #     training_sessions = training_sessions.filter(project__in=projects)
        #     usage_events = usage_events.filter(project__in=projects)
        # if user:
        #     area_access = area_access.filter(customer=user)
        #     consumables = consumables.filter(customer=user)
        #     missed_reservations = missed_reservations.filter(user=user)
        #     staff_charges = staff_charges.filter(customer=user)
        #     training_sessions = training_sessions.filter(trainee=user)
        #     usage_events = usage_events.filter(user=user)
        # if bool(request.GET.get("csv", False)):
        #     return csv_export_response(
        #         request.user,
        #         usage_events,
        #         area_access,
        #         training_sessions,
        #         staff_charges,
        #         consumables,
        #         missed_reservations,
        #     )
    except:
        pass
    # dictionary = {
    #     "search_items": set(Account.objects.all())
    #     | set(Project.objects.all())
    #     | set(get_project_applications())
    #     | set(User.objects.all()),
    #     "area_access": area_access,
    #     "consumables": consumables,
    #     "missed_reservations": missed_reservations,
    #     "staff_charges": staff_charges,
    #     "training_sessions": training_sessions,
    #     "usage_events": usage_events,
    #     "project_autocomplete": True,
    #     "selection": selection,
    # }
    # dictionary["no_charges"] = not (
    #     dictionary["area_access"]
    #     or dictionary["consumables"]
    #     or dictionary["missed_reservations"]
    #     or dictionary["staff_charges"]
    #     or dictionary["training_sessions"]
    #     or dictionary["usage_events"]
    # )

    item = base_dictionary["item"]
    user = base_dictionary["customer"]

    dictionary = get_usage(start_date, end_date, user=user, projects=projects, item=item)
    dictionary["project_autocomplete"] = True
    dictionary["selection"] = selection
    dictionary["no_charges"] = not (
        dictionary["area_access"]
        or dictionary["stockroom_purchases"]
        or dictionary["missed_reservations"]
        or dictionary["staff_charges"]
        or dictionary["training_sessions"]
        or dictionary["usage_events"]
    )
    return render(request, "usage/usage.html", {**base_dictionary, **dictionary})


@accounting_or_user_office_or_manager_required
@require_GET
def project_billing(request):
    base_dictionary, start_date, end_date, kind, identifier = date_parameters_dictionary(request, get_day_timeframe)
    if not base_dictionary["billing_service"]:
        return redirect("project_usage")
    base_dictionary["project_autocomplete"] = True
    base_dictionary["search_items"] = (
        set(Account.objects.all())
        | set(Project.objects.all())
        | set(get_project_applications())
        | set(User.objects.all())
    )

    project_id = None
    account_id = None
    user = None
    formatted_applications = None
    selection = ""
    try:
        if kind == "application":
            formatted_applications = identifier
            selection = identifier
        elif kind == "project":
            projects = [Project.objects.get(id=identifier)]
            formatted_applications = projects[0].application_identifier
            project_id = identifier
            selection = projects[0].name
        elif kind == "account":
            account = Account.objects.get(id=identifier)
            projects = Project.objects.filter(account=account, active=True, account__active=True)
            formatted_applications = (
                ",".join(map(str, set(projects.values_list("application_identifier", flat=True)))) if projects else None
            )
            account_id = account.id
            selection = account.name
        elif kind == "user":
            user = User.objects.get(id=identifier)
            projects = user.active_projects()
            formatted_applications = (
                ",".join(map(str, set(projects.values_list("application_identifier", flat=True)))) if projects else None
            )
            selection = str(user)

        base_dictionary["selection"] = selection
        billing_dictionary = billing_dict(
            start_date,
            end_date,
            user,
            formatted_applications,
            project_id,
            account_id=account_id,
            force_pi=True if not user else False,
        )
        return render(request, "usage/billing.html", {**base_dictionary, **billing_dictionary})
    except Exception as e:
        logger.warning(str(e))
        return render(request, "usage/billing.html", base_dictionary)


def is_user_pi(user: User, latest_pis_data, activity, user_managed_applications: List[str]):
    # Check if the user is set as a PI in NEMO, otherwise check from latest_pis_data
    application = activity["application_name"]
    if application in user_managed_applications:
        return True
    else:
        application_pi_row = next((x for x in latest_pis_data if x["application_name"] == application), None)
        return application_pi_row is not None and (
            user.username == application_pi_row["username"]
            or (
                user.first_name == application_pi_row["first_name"]
                and user.last_name == application_pi_row["last_name"]
            )
        )


def billing_dict(start_date, end_date, user, formatted_applications, project_id=None, account_id=None, force_pi=False):
    # The parameter force_pi allows us to display information as if the user was the project pi
    # This is useful on the admin project billing page tp display other project users for example
    dictionary = {}

    billing_service = get_billing_service()
    if not billing_service.get("available", False):
        return dictionary

    cost_activity_url = billing_service["cost_activity_url"]
    project_lead_url = billing_service["project_lead_url"]
    keyword_arguments = billing_service["keyword_arguments"]

    cost_activity_params = {
        "created_date_gte": f"'{start_date.strftime('%m/%d/%Y')}'",
        "created_date_lt": f"'{end_date.strftime('%m/%d/%Y')}'",
        "application_names": f"'{formatted_applications}'",
        "$format": "json",
    }
    cost_activity_response = get(cost_activity_url, params=cost_activity_params, **keyword_arguments)
    cost_activity_data = cost_activity_response.json()["d"]

    if not force_pi:
        latest_pis_params = {"$format": "json"}
        latest_pis_response = get(project_lead_url, params=latest_pis_params, **keyword_arguments)
        latest_pis_data = latest_pis_response.json()["d"]

    project_totals = {}
    application_totals = {}
    account_totals = {}
    user_pi_applications = list()
    # Construct a tree of account, application, project, and member total spending
    cost_activities_tree = {}
    user_managed_applications = (
        [project.application_identifier for project in user.managed_projects.all()] if not force_pi else []
    )
    for activity in cost_activity_data:
        if (project_id and activity["project_id"] != str(project_id)) or (
            account_id and activity["account_id"] != str(account_id)
        ):
            continue
        project_totals.setdefault(activity["project_id"], 0)
        application_totals.setdefault(activity["application_id"], 0)
        account_totals.setdefault(activity["account_id"], 0)
        account_key = (activity["account_id"], activity["account_name"])
        application_key = (activity["application_id"], activity["application_name"])
        project_key = (activity["project_id"], activity["project_name"])
        user_key = (activity["member_id"], User.objects.filter(id__in=[activity["member_id"]]).first())
        user_is_pi = is_user_pi(user, latest_pis_data, activity, user_managed_applications) if not force_pi else True
        if user_is_pi:
            user_pi_applications.append(activity["application_id"])
        if user_is_pi or str(user.id) == activity["member_id"]:
            cost_activities_tree.setdefault((activity["account_id"], activity["account_name"]), {})
            cost_activities_tree[account_key].setdefault(application_key, {})
            cost_activities_tree[account_key][application_key].setdefault(project_key, {})
            cost_activities_tree[account_key][application_key][project_key].setdefault(user_key, 0)
            cost = 0
            if activity["cost"] is not None:
                cost = -activity["cost"] if activity["activity_type"] == "refund_activity" else activity["cost"]
            cost_activities_tree[account_key][application_key][project_key][user_key] = (
                cost_activities_tree[account_key][application_key][project_key][user_key] + cost
            )
            project_totals[activity["project_id"]] = project_totals[activity["project_id"]] + cost
            application_totals[activity["application_id"]] = application_totals[activity["application_id"]] + cost
            account_totals[activity["account_id"]] = account_totals[activity["account_id"]] + cost
    dictionary["spending"] = (
        {
            "activities": cost_activities_tree,
            "project_totals": project_totals,
            "application_totals": application_totals,
            "account_totals": account_totals,
            "user_pi_applications": user_pi_applications,
        }
        if cost_activities_tree
        else {"activities": {}}
    )
    return dictionary


def csv_export_response(
    user: User, usage_events, area_access, training_sessions, staff_charges, consumables, missed_reservations
):
    table_result = BasicDisplayTable()
    table_result.add_header(("type", "Type"))
    table_result.add_header(("user", "User"))
    table_result.add_header(("name", "Item"))
    table_result.add_header(("details", "Details"))
    table_result.add_header(("project", "Project"))
    if user.is_any_part_of_staff:
        table_result.add_header(
            ("application", ProjectsAccountsCustomization.get("project_application_identifier_name"))
        )
    table_result.add_header(("start", "Start time"))
    table_result.add_header(("end", "End time"))
    table_result.add_header(("quantity", "Quantity"))
    data: List[BillableItem] = []
    data.extend(billable_items_missed_reservations(missed_reservations))
    data.extend(billable_items_consumable_withdrawals(consumables))
    data.extend(billable_items_staff_charges(staff_charges))
    data.extend(billable_items_training_sessions(training_sessions))
    data.extend(billable_items_area_access_records(area_access))
    data.extend(billable_items_usage_events(usage_events))
    for billable_item in data:
        table_result.add_row(vars(billable_item))
    response = table_result.to_csv()
    filename = f"usage_export_{export_format_datetime()}.csv"
    response["Content-Disposition"] = f'attachment; filename="{filename}"'
    return response


def get_managed_projects(user: User) -> Set[Project]:
    # This function will get managed projects from NEMO and also attempt to get them from billing service
    managed_projects = set(list(user.managed_projects.all()))
    billing_service = get_billing_service()
    if billing_service.get("available", False):
        # if we have a billing service, use it to determine project lead
        project_lead_url = billing_service["project_lead_url"]
        keyword_arguments = billing_service["keyword_arguments"]
        latest_pis_params = {"$format": "json"}
        latest_pis_response = get(project_lead_url, params=latest_pis_params, **keyword_arguments)
        latest_pis_data = latest_pis_response.json()["d"]
        for project_lead in latest_pis_data:
            if project_lead["username"] == user.username or (
                project_lead["first_name"] == user.first_name and project_lead["last_name"] == user.last_name
            ):
                try:
                    for managed_project in Project.objects.filter(
                        application_identifier=project_lead["application_name"]
                    ):
                        managed_projects.add(managed_project)
                except Project.DoesNotExist:
                    pass
    return managed_projects


def get_billing_service():
    return getattr(settings, "BILLING_SERVICE", {})
