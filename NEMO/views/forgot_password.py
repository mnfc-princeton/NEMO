from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.http import require_http_methods

from NEMO.models import ForgotPasswordToken, User, UserAuth
from NEMO.utilities import EmailCategory, render_email_template, send_mail
from NEMO.views.customization import EmailsCustomization, get_media_file_contents


@require_http_methods(["GET"])
def forgot_password(request):
    return render(request, "db_authentication/forgot_password.html", {})


@require_http_methods(["POST"])
def forgot_password_process(request):
    username = request.POST.get("username")
    subject = "NEMO Password Reset"
    try:
        user = User.objects.get(username=username)
        email = user.email
        message = get_media_file_contents("forgot_password_email.html")
        token = ForgotPasswordToken.create(username)
        token.save()
        link = request.build_absolute_uri(reverse("password_reset_token", args=[token.hash]))
        dictionary = {"link": link}
        rendered_message = render_email_template(message, dictionary, request)
        user_office_email = EmailsCustomization.get("user_office_email_address")
        send_mail(
            subject=subject,
            content=rendered_message,
            from_email=user_office_email,
            to=[email],
            email_category=EmailCategory.SYSTEM,
        )
    except User.DoesNotExist:
        pass

    return render(request, "db_authentication/forgot_password_process.html")


@require_http_methods(["GET", "POST"])
def password_reset_token(request, token):
    try:
        token = ForgotPasswordToken.objects.get(hash=token)
    except ForgotPasswordToken.DoesNotExist:
        token = None

    if request.method == "GET":
        if token and token.is_valid():
            return render(request, "db_authentication/password_reset_token.html", {})
        else:
            return render(request, "db_authentication/password_reset_token_expired.html", {})
    else:
        if token and token.is_valid():
            reset_password_by_token(token, request.POST.get("password"))
            return render(request, "db_authentication/password_reset_token_success.html", {})
        else:
            return render(request, "db_authentication/password_reset_token_expired.html", {})


def reset_password_by_token(token: ForgotPasswordToken, password):
    user = User.objects.get(username=token.username)
    user_auth, created = UserAuth.objects.get_or_create(username=user.username)
    user_auth.set_password(password)
    user_auth.save()
    token.expired = True
    token.save()
